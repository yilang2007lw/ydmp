//
//  LocalMovieViewController.swift
//  ydmp
//
//  Created by yilang on 15/4/15.
//  Copyright (c) 2015年 yilang. All rights reserved.
//

import UIKit
import MediaPlayer

class LocalMovieViewController: UITableViewController {

    var films = [AnyObject]()
    var player:MPMoviePlayerController = MPMoviePlayerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        var movieQuery = MPMediaQuery()
        var predicate = MPMediaPropertyPredicate(value:MPMediaType.HomeVideo.rawValue, forProperty: MPMediaItemPropertyMediaType)
        movieQuery.addFilterPredicate(predicate)
        films = movieQuery.items
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return films.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("movieCell", forIndexPath: indexPath) as! UITableViewCell
        var item:MPMediaItem = films[indexPath.row] as! MPMediaItem
        
        cell.textLabel?.text = item.valueForProperty(MPMediaItemPropertyTitle) as? String

        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var item:MPMediaItem = films[indexPath.row] as! MPMediaItem
        player.contentURL = item.valueForProperty(MPMediaItemPropertyAssetURL) as! NSURL
        player.prepareToPlay()
        player.view.frame = view.bounds
        view.addSubview(player.view)
        player.play()
    }

}
