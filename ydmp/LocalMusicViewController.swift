//
//  LocalMusicViewController.swift
//  ydmp
//
//  Created by yilang on 15/4/15.
//  Copyright (c) 2015年 yilang. All rights reserved.
//

import UIKit
import MediaPlayer

class LocalMusicViewController: UITableViewController {
    var songs = [AnyObject]()
    var player:MPMusicPlayerController = MPMusicPlayerController.applicationMusicPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        var musicQuery = MPMediaQuery.songsQuery()
        songs = musicQuery.items
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("musicCell", forIndexPath: indexPath) as! UITableViewCell
        
        var item:MPMediaItem = songs[indexPath.row] as! MPMediaItem
        var itemArtwork:MPMediaItemArtwork? = item.valueForProperty(MPMediaItemPropertyArtwork) as? MPMediaItemArtwork
        if let artwork = itemArtwork {
            cell.imageView!.image = artwork.imageWithSize(CGSizeMake(50, 50))
        } else {
            cell.imageView!.image = UIImage(named: "music.png")
        }
        
        cell.textLabel?.text = item.valueForProperty(MPMediaItemPropertyTitle) as? String
        
        cell.detailTextLabel?.text = "\(item.valueForProperty(MPMediaItemPropertyArtist) as! String) \(item.valueForProperty(MPMediaItemPropertyAlbumTitle) as! String)"
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var item:MPMediaItem = songs[indexPath.row] as! MPMediaItem
        player.setQueueWithItemCollection(MPMediaItemCollection(items: songs))
        player.nowPlayingItem = item
        player.prepareToPlay()
        player.play()
    }
    
}
