//
//  LocalViewController.swift
//  ydmp
//
//  Created by yilang on 15/4/15.
//  Copyright (c) 2015年 yilang. All rights reserved.
//

import UIKit

class LocalViewController: UITableViewController {

    @IBOutlet weak var musicCell: UITableViewCell!
    
    @IBOutlet weak var movieCell: UITableViewCell!
    
    @IBOutlet weak var tvshowCell: UITableViewCell!
    
    @IBOutlet weak var itunesuCell: UITableViewCell!
    
    @IBOutlet weak var podcastCell: UITableViewCell!
    
    @IBOutlet weak var audiobookCell: UITableViewCell!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
